# Is my airline still flying 737 MAX 8?

This project contains the code that powers [737max8.com](https://737max8.com). Content from [airlines.json](https://gitlab.com/ClemMakesApps/737-max-8/blob/master/data/airlines.json) and [countries.json](https://gitlab.com/ClemMakesApps/737-max-8/blob/master/data/countries.json) is used to generate the website using GitLab CI.

## How to contribute (non-technical)
1. Create an [issue](https://gitlab.com/ClemMakesApps/737-max-8/issues/new) in this project
1. Detail the name of the airline, info on whether it is flying 737 MAX 8 and a link to a reputable source. If a country has announced that they are grounding their 737 MAX 8 planes, please detail the reputable source.

## How to contribute (technical)
1. Fork this project
1. Update [airlines.json](https://gitlab.com/ClemMakesApps/737-max-8/blob/master/data/airlines.json) with your airline information or [countries.json](https://gitlab.com/ClemMakesApps/737-max-8/blob/master/data/countries.json) with your country information
1. Create a Merge Request in this project
