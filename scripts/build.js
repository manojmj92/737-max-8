const pug = require('pug');
const fs = require('fs');
const compile = pug.compileFile('./templates/index.pug');

fs.readFile('data/countries.json', 'utf8', function (err, countriesData) {
  if (err) throw err;
  let countries = JSON.parse(countriesData);
  console.log(countries)
  fs.readFile('data/airlines.json', 'utf8', function (err, data) {
    if (err) throw err;
    let airlines = JSON.parse(data);
    airlines = airlines.sort(function (a, b) {
      return a.name > b.name;
    });

    console.log(airlines)
    fs.writeFile('public/index.html', compile({
      airlines,
      countries
    }), function(err) {
      if(err) {
        return console.log(err);
      }
      console.log("The file was saved!");
    });
  });
});
